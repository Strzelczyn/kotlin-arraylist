import java.io.File

fun readFromFile(): List<String> {
    var path = "E:fileToRead.txt"
    var file = File(path)
    return file.readLines()
}

fun convertReadedValuesToArrayList(values: List<String>): ArrayList<Int> {
    var convertedData = arrayListOf<Int>()
    for (i in values) {
        convertedData.add(i.toInt())
    }
    return convertedData
}

fun main(args: Array<String>) {
    var readedValues = readFromFile()
    var arrayListValues = convertReadedValuesToArrayList(readedValues)
    print("Sum of arrayList is " + arrayListValues.sum())
}
